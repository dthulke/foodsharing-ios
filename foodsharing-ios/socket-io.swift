//
//  socket-io.swift
//  foodsharing-ios
//
//  Created by Baudisgroup User on 23.04.19.
//

import Foundation
import SocketIO

class SocketIOManager: NSObject {

    static let sharedInstance = SocketIOManager()

    var manager = SocketManager(socketURL: URL(string: Constants.base_url)!, config: [.path("/chat/socket.io/"), .log(true), .compress, .forceWebsockets(true)])

    lazy var socket = manager.defaultSocket

    override init() {
        super.init()

        socket.on("connect", callback: { (data, ack) in

            self.socket.emit("register")
        })


        socket.on("conv") { (data, ack) in

            NotificationCenter.default.post(name: .gotConvFromSocket, object: data)

        }

    }

    func establishConnection() {
        socket.connect()
    }

    func closeConnection() {
        socket.disconnect()
    }

    func onclickbutton() {
        socket.emit("message", "GEKK")
    }
}
