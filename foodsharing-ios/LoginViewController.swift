//
//  ViewController.swift
//  LoginScreenSwift
//


import UIKit

@available(iOS 10.0, *)
class loginViewController: UIViewController {


    @IBOutlet weak var fringe: UIView!

    @IBOutlet weak var errorField: UILabel!

    @IBOutlet weak var email: PaddingTextField!{
        didSet {
            email.delegate = self
        }
    }
    @IBOutlet weak var passwordTF: PaddingTextField!{
        didSet {
            passwordTF.delegate = self
        }
    }
    @IBOutlet weak var login_button: UIButton!


    var activeTextField = UITextField() {
        didSet {
            activeTextField.activateBottomBorder()
        }
    }

    var inactiveTextField = UITextField() {
        didSet {
            inactiveTextField.deactivateBottomBorder()
        }
    }




    var detail: [String:Any] = [:]

    // make time battery white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    var passwordVisible = false
    let visibleButton = UIButton(type: .custom)

    override func viewDidLoad() {


        NotificationCenter.default.addObserver(self, selector: #selector(self.OnDidLogin(notification:)), name: .login_success, object: nil)


        email.addDefaultBottomBorder()
        passwordTF.addDefaultBottomBorder()
        login_button.layer.cornerRadius = 4
        login_button.clipsToBounds = true


        passwordTF.rightView = visibleButton
        passwordTF.rightViewMode = .unlessEditing //.always

        visibleButton.setImage(UIImage(named: "visible.png"), for: .normal)
        visibleButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: -6, bottom: 0, right: 6)
        visibleButton.frame = CGRect(x: CGFloat(passwordTF.frame.size.width), y: CGFloat(3), width: CGFloat(16), height: CGFloat(16))
//        button.addTarget(self, action: #selector(self.showPassword), for: .touchDown)
        visibleButton.addTarget(self, action: #selector(self.togglePassword), for: .touchUpInside)

    }

    @IBOutlet weak var fringeHeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var fringeFSspacingConstraint: NSLayoutConstraint!


    @IBOutlet weak var FSErrorConstraint: NSLayoutConstraint!

    override func viewWillLayoutSubviews() {

        fringeHeightConstraint.constant = self.view.frame.size.height / 7
        fringeFSspacingConstraint.constant =
        (errorField.frame.minY - fringe.frame.maxY - CGFloat(Constants.FoodSharing_logo_height)) * 2/5

        FSErrorConstraint.constant =        (errorField.frame.minY - fringe.frame.maxY - CGFloat(Constants.FoodSharing_logo_height)) * 3/5
    }

    @objc func OnDidLogin(notification: Notification) {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "tab_view", sender: self)
        }
    }

    @objc func togglePassword() {
        if self.passwordVisible {

            self.visibleButton.setImage(UIImage(named: "visible.png"), for: .normal)

        } else {

            self.visibleButton.setImage(UIImage(named: "visible_true.png"), for: .normal)

        }
        self.passwordVisible = !self.passwordVisible
        self.passwordTF.isSecureTextEntry = !self.passwordTF.isSecureTextEntry
    }



    @IBAction func LoginButtonClicked(_ sender: Any) {
        let username = email.text
        let password = passwordTF.text

        if(username == "" || password == "")
        {
            return
        }


        login(user: username!, psw: password!)


    }



}
extension loginViewController {
    func login(user: String, psw: String) {
        fetch_token(user: user, psw: psw) { (error: loginError?) in
            if let error = error{
//                print ("error login?")
                switch error {
                case .wrongPassword:
                    // change log in view
                    DispatchQueue.main.async {

                        self.errorField.text = "Email and password don't match."

                    }

                case .noConnection:
                    // change log in view
                    DispatchQueue.main.async {
                        self.errorField.text = "Connection failure."
                    }
                }

            } else {
//                print("token fetched!")
                NotificationCenter.default.post(name: .login_success, object: nil)
            }

        }



    }
}

extension loginViewController: UITextFieldDelegate {

    // Assign the newly active text field to your activeTextField variable
    func textFieldDidBeginEditing(_ textField: UITextField) {

        self.activeTextField = textField
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        self.inactiveTextField = textField
    }
}
