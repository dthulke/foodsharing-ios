//
//  NavigationController.swift
//  foodsharing-ios
//
//  Created by Baudisgroup User on 27.04.19.
//

import Foundation
import UIKit

class navigationController: UINavigationController {
    
    // make time battery white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        self.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
        self.navigationBar.tintColor = UIColor.white
        self.navigationItem.hidesBackButton = true
        
    }
    
    
}

